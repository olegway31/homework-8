const fs = require('fs');
const path = require('path');

function readFile() {
  try {
    return fs.readFileSync(path.join(__dirname, 'db/db.json'), 'utf8');
  } catch (err) {
    console.error(err);
  }
}

module.exports = {
  readFile,
};
