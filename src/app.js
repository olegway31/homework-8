// import readFile from './db';


const config = global.config;

const db = require('./db');
const path = require('path');

const express = require('express')
const expressLogging = require('express-logging')
const logger = require('logops');

const cookieParser = require("cookie-parser");
const sessions = require('express-session');


const app = express();

const md5 = require('md5');
const oneDay = 1000 * 60 * 60 * 24;

global.session;  // глобальное хранение сессии

// view engine setup
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'pages'));

//session middleware
app.use(sessions({
  secret: "thisismysecrctekeyfhrgfgrfrty84fwir767",
  saveUninitialized: true,
  cookie: { maxAge: oneDay },
  resave: false
}));


app.use(express.static(path.join(__dirname, '/db')));
app.use(express.static(path.join(__dirname, '/static')));

app.use(expressLogging(logger));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// cookie parser middleware
app.use(cookieParser());

app.get('/login', (req, res) => {
  res.render('auth', { message: '' });
});

app.get('/', (req, res) => {
  global.session = req.session;
  if (global.session.userid) {
    res.render('game');
  } else res.redirect('/login')
})

app.get('/logout', (req, res) => {
  req.session.destroy();
  res.redirect('/');
});

// Когда нажали кнопку <войти> 
app.post('/login', (req, res) => {

  const userLogin = req.body.login
  const userPassword = md5(req.body.password)

  if (hasMatchCredentials({ userLogin, userPassword })) {
    global.session = req.session;
    global.session.userid = userLogin
    res.render('game', { message: "User registered!" })
  } else res.render('auth', { message: "un User registered!" })
});


// Проверка сессии. Если нет. Редирест на вход. Иначе. Можем выполнять код дальше
app.use((req, res, next) => {
  console.log('req.session.userid -> ', req.session.userid)
  if (req.session.userid) { next(); }
  else res.redirect('/login');
});

app.get('/game', (req, res) => {
  res.render('game');
});

app.get('/rating', (req, res) => {
  res.render('rating');
});

// Запуск сервера на порту объявленном в конфигах
app.listen(config.port, () => {
  console.log(`Server has started on port ${config.port}...`);
});

// Функция проверки есть ли пользователь в "бд" и совпадает ли хэш пароля с формы с хэшом из бд
function hasMatchCredentials({ userLogin, userPassword }) {
  const usersCredentials = JSON.parse(db.readFile());
  for (let user of usersCredentials) {
    if (user.login === userLogin && user.passwordHash === userPassword) {
      console.log(`${user.login} === ${userLogin} && ${user.passwordHash} === ${userPassword} -> равны`);
      return true
    }
  }
  return false
}

// var $ = cheerio.load(data);

// $(".some-class").text("returned user input: " + userInput);
// res.send($.html());