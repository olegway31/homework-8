const loginInput = document.querySelector('input[name="login"]');
const passwordInput = document.querySelector('input[name="password"]');
const MIN_PASS_LENGTH = 4;
let submit = document.querySelector('input[type="submit"]');
// submit.disabled = true;
window.onload = () => {
  if (passwordInput.value) {
    submit.disabled = false
    submit.classList.add('active');
  } else {
    submit.disabled = true
    submit.classList.remove('active');
  } 
}

loginInput.addEventListener('input', function (event) {
  const regex = /^[A-Za-z0-9._]*$/;
  loginInput.classList.remove('wrongInputColor');
  if (!regex.test(loginInput.value)) {
    loginInput.value = loginInput.value.replace(/[^A-Za-z0-9._]/g, '');
    loginInput.classList.add('wrongInputColor');
  }
});

passwordInput.addEventListener('input', function (event) {
  passwordInput.classList.remove('wrongInputColor');
  if (hasMinLengthField(passwordInput.value)) {
    submit.disabled = false;
    submit.classList.add('active');
  } else {
    submit.classList.remove('active');
    submit.disabled = true;
  }
  const regex = /^[A-Za-z0-9._]*$/;
  if (!regex.test(passwordInput.value)) {
    passwordInput.value = passwordInput.value.replace(/[^A-Za-z0-9._]/g, '');
    passwordInput.classList.add('wrongInputColor');
  }
});

// запрет копирования
loginInput.addEventListener('copy', function (event) {
  event.preventDefault();
});

passwordInput.addEventListener('copy', function (event) {
  event.preventDefault();
});

//
submit.addEventListener('click', function (event) {
  // event.preventDefault();
  console.log(`Login: ${loginInput.value}, password: ${passwordInput.value}`);
});

function hasMinLengthField(fieldValue) {
  return fieldValue.length >= MIN_PASS_LENGTH;
}
